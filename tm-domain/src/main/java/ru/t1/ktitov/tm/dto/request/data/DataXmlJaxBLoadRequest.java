package ru.t1.ktitov.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.AbstractUserRequest;

public final class DataXmlJaxBLoadRequest extends AbstractUserRequest {

    public DataXmlJaxBLoadRequest(@Nullable final String token) {
        super(token);
    }

}
