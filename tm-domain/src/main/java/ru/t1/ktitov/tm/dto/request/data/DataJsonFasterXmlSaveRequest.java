package ru.t1.ktitov.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.AbstractUserRequest;

public final class DataJsonFasterXmlSaveRequest extends AbstractUserRequest {

    public DataJsonFasterXmlSaveRequest(@Nullable final String token) {
        super(token);
    }

}
