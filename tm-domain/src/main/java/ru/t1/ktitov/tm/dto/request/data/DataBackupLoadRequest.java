package ru.t1.ktitov.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.AbstractUserRequest;

public final class DataBackupLoadRequest extends AbstractUserRequest {

    public DataBackupLoadRequest(@Nullable final String token) {
        super(token);
    }

}
